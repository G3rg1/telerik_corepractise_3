﻿using System;
using System.Linq;

namespace Matrix_Max_Sum
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var size = int.Parse(Console.ReadLine());

            var strInputMtrx = new string[size];

            for (int i = 0; i < size; i++)
            {
                strInputMtrx[i] = Console.ReadLine();
            }

            var mtrx = GenerateMatrix(size, strInputMtrx);

            //var mtrx = new int[,]{
            //    { 1,22,3,41,5,2},
            //    { 2,13,4,-5,6,5},
            //    { -6,5,9,31,2,8},
            //    { 3,14,5,-6,7,4},
            //    { 4,-5,6,-7,8,7}
            //    };

            var maxSum = int.MinValue;

            var inputPairs = Console.ReadLine();
            var coordPairs =  inputPairs.Split(' ').Select(int.Parse).ToArray(); //new int[] { -3, -3, 3, 3, 4, -3, -4, 3 };

            for (int i = 0; i < coordPairs.Length; i++)
            {
                var rowCoord = coordPairs[i];
                i++;
                var colCoord = coordPairs[i];

                var row = Math.Abs(rowCoord) - 1;
                var col = Math.Abs(colCoord) - 1;
                var start = 0;
                var stop = 0;
                var currentSum = 0;

                if (rowCoord > 0)
                {
                    stop = col;

                    for (int j = start; j <= stop; j++)
                    {
                        currentSum += mtrx[row, j];

                        if (j == stop)
                        {
                            col = j;
                        }
                    }

                    start = row;

                    if (colCoord > 0)
                    {
                        stop = 0;

                        for (int j = start - 1; j >= stop; j--)
                        {
                            currentSum += mtrx[j, col];
                        }
                    }
                    else
                    {
                        stop = mtrx.GetLength(0) - 1;

                        for (int j = start + 1; j <= stop; j++)
                        {
                            currentSum += mtrx[j, col];
                        }
                    }
                }
                else
                {
                    start = mtrx.GetLength(1) - 1;
                    stop = col;

                    for (int j = start; j >= stop; j--)
                    {
                        currentSum += mtrx[row, j];

                        if (j == stop)
                        {
                            col = j;
                        }
                    }

                    start = row;

                    if (colCoord > 0)
                    {
                        stop = 0;

                        for (int j = start - 1; j >= stop; j--)
                        {
                            currentSum += mtrx[j, col];
                        }
                    }
                    else
                    {
                        stop = mtrx.GetLength(0) - 1;

                        for (int j = start + 1; j <= stop; j++)
                        {
                            currentSum += mtrx[j, col];
                        }
                    }
                }

                if (currentSum > maxSum)
                {
                    maxSum = currentSum;
                }
            }

            //if (maxSum == 0)
            //{
            //    Console.WriteLine($":{strInputMtrx[0]}");
            //}
            //else
            //{
                
            //}
            Console.WriteLine(maxSum);
        }
        static int[,] GenerateMatrix(int size, string[] strInput)
        {
            var mtrx = new int[0, 0];

            bool isInitated = false;
            for (int i = 0; i < size; i++)
            {

                var currentRow = strInput[i].Split(' ').Select(int.Parse).ToArray();

                if (isInitated == false)
                {
                    mtrx = new int[size, currentRow.Length];
                    isInitated = true;
                }

                for (int j = 0; j < currentRow.Length; j++)
                {
                    mtrx[i, j] = currentRow[j];
                }
            }
            return mtrx;
        }
    }
}