﻿using System;
using System.Linq;
using System.Numerics;

namespace Zig_Zag
{
    class Program
    {
        static void Main(string[] args)
        {
            var dimention = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            //value to add for each 2 diagonals
            BigInteger addValue = ((dimention[1] - 1) * 2) * 2;
            BigInteger smallestValue = addValue / 2;
            BigInteger totAddValue = 0;
            BigInteger lastValue = 0;
            //BigInteger apexValue = ((dimention[0] - 1) + (dimention[1] - 1)) * 3 + 1;

            for (int i = 1; i <= ((dimention[0] - 1) / 2) - 1; i++)
            {
                BigInteger currAddValue = addValue * i;
                totAddValue += currAddValue;

                if (i == ((dimention[0] - 1) / 2) - 1)
                {
                    lastValue = smallestValue * (i + 1);
                }
            }

            BigInteger totalCalcs = (dimention[0] - 1) * (dimention[1] - 1);
            if(dimention[0] % 2 == 0)
            {
                totAddValue += (lastValue);
            }
            else if(dimention[0] <= 3)
            {
                addValue = 0;
            }

            //sum for each row
            BigInteger rowSum = dimention[1] * (dimention[1] / 2);
            //sum of all rows
            BigInteger sumRows = rowSum * (dimention[0] - 1);
            //resulting sum
            BigInteger sum = ( (sumRows + (totAddValue) ) * 3) + totalCalcs  + 1;

            Console.WriteLine(sum);
        }
    }
}
