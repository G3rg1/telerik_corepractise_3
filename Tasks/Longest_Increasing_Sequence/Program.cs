﻿using System;

class Program
{
    static void Main()
    {
        var n = int.Parse(Console.ReadLine());
        var arr = new int[n];

        for (int i = 0; i < n; i++)
        {
            arr[i] = int.Parse(Console.ReadLine());
        }


        var currentStreak = 1;
        var longestStreak = 1;

        for (int i = 0; i < arr.Length - 1; i++)
        {
            if (arr[i] < arr[i + 1])
            {
                currentStreak++;
            }
            else
            {
                if (currentStreak > longestStreak)
                {
                    longestStreak = currentStreak;
                }

                currentStreak = 1;
            }

            if (currentStreak > longestStreak)
            {
                longestStreak = currentStreak;
            }
        }

        Console.WriteLine(longestStreak);
    }
}