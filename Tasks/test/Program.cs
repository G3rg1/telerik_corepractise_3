﻿using System;
using System.Linq;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            var dimention = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            var mtrx = GenerateMtrx(dimention);
            PrintMtrx(mtrx);
            var sum = 0;
            for (int i = 0; i < mtrx.GetLength(0); i++)
            {
                for (int j = 0; j < mtrx.GetLength(1); j++)
                {
                    if (mtrx[i, j] % 2 != 0)
                    {
                        sum += mtrx[i, j];
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Sum : {sum}");
        }

        static int[,] GenerateMtrx(int[] dimention)
        {
            var mtrx = new int[dimention[0], dimention[1]];
            int j = 0;
            var value = 1;

            for (int i = 0; i < dimention[0]; i++)
            {
                mtrx[i, j] = value;

                int colValue = value;

                for (j = 1; j < dimention[1]; j++)
                {
                    colValue += 3;
                    mtrx[i, j] = colValue;
                }

                value += 3;
                j = 0;
            }
            return mtrx;
        }
        static void PrintMtrx(int[,] mtrx)
        {
            for (int i = 0; i < mtrx.GetLength(0); i++)
            {
                for (int j = 0; j < mtrx.GetLength(1); j++)
                {
                    Console.Write(mtrx[i, j].ToString().PadLeft(3));
                }
                Console.WriteLine();
            }
        }
    }
}
