﻿using System;

namespace Spiral_Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());

            var mtrx = GenerateMtrx(n);
            
            PringMtrx(mtrx);
        }
        static int[,] GenerateMtrx(int n)
        {
            int[] dx = { 0, 1, 0, -1 }; 
            int[] dy = { 1, 0, -1, 0 };

            int x = 0;
            int y = -1;
            int c = 0;

            int[,] mtrx = new int[n, n];

            for (int i = 0, im = 0; i < n + n - 1; ++i, im = i % 4)
            {
                for (int j = 0, jlen = (n + n - i) / 2; j < jlen; ++j)
                {
                    mtrx[x += dx[im], y += dy[im]] = ++c;
                }
                    
            }
                

            return mtrx;
        }
        static void PringMtrx(int[,] mtrx)
        {
            for (int i = 0; i < mtrx.GetLength(0); i++)
            {
                for (int j = 0; j < mtrx.GetLength(1); j++)
                {
                    Console.Write($" {mtrx[i, j]}");
                }
                Console.WriteLine();
            }
        }
    }
}
