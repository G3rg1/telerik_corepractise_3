﻿using System;
using System.Text;
 
namespace MethodsDemoA41
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            int[,] matrix = new int[n, n];
 
            int[] dirRows = { -2, -2, -1, -1, +1, +1, +2, +2 };
            int[] dirCols = { -1, +1, -2, +2, -2, +2, -1, +1 };
 
            int count = 1;
 
            for (int r = 0; r < matrix.GetLength(0); r++)
            {
                for (int c = 0; c < matrix.GetLength(1); c++)
                {
                    int row = r;
                    int col = c;
 
                    while (matrix[row, col] == 0)
                    {
                        matrix[row, col] = count;
                        count++;
 
                        for (int dir = 0; dir < dirRows.Length; dir++)
                        {
                            int nextRow = row + dirRows[dir];
                            int nextCol = col + dirCols[dir];
 
                            if (nextRow < 0 || nextRow >= n
                                || nextCol < 0 || nextCol >= n)
                            {
                                continue;
                            }
 
                            if (matrix[nextRow, nextCol] != 0)
                            {
                                continue;
                            }
 
                            row = nextRow;
                            col = nextCol;
                            break;
                        }
                    }
                }
            }
 
            for (int r = 0; r < matrix.GetLength(0); r++)
            {
                for (int c = 0; c < matrix.GetLength(1); c++)
                {
                    Console.Write(matrix[r,c] + " ");
                }
                Console.WriteLine();
            }
        }
 
    }
}
 