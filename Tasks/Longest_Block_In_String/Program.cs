﻿using System;
using System.Linq;

namespace Longest_Block_In_String
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine();

            var longestRun = new string(input.Select((c, index) => input.Substring(index)
                                   .TakeWhile(e => e == c))
                                   .OrderByDescending(e => e.Count())
                                   .First().ToArray());

            Console.WriteLine(longestRun);
        }
    }
}