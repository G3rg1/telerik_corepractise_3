﻿using System;
using System.Linq;

namespace Matrix_Max_Sum_2
{
    internal class Program
    {
        static void Main(string[] args)
        {

            var size = 5;//int.Parse(Console.ReadLine());

            if (size >= 5 && size <= 20)
            {
                //var mtrx = GenerateMatrix(size);

                var mtrx = new int[,]{
                { 1,22,3,41,5,2},
                { 2,13,4,-5,6,5},
                { -6,5,9,31,2,8},
                { 3,14,5,-6,7,4},
                { 4,-5,6,-7,8,7}
                };

                var maxSum = 0;

                var coordPairs = new int[] { -3, -3, 3, 3, 4, -3, -4, 3 };//Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

                for (int i = 0; i < coordPairs.Length; i += 2)
                {
                    int horizontalDirection = 1;

                    if (coordPairs[i] < 0)
                    {
                        horizontalDirection = -1;
                    }

                    int verticalDirection = -1;

                    if (coordPairs[i + 1] < 0)
                    {
                        verticalDirection = 1;
                    }

                    int row = Math.Abs(coordPairs[i]) - 1;

                    int targetRow = verticalDirection < 0 ? 0 : mtrx.GetLength(0) - 1;

                    int col = horizontalDirection < 0 ? mtrx.GetLength(1) - 1 : 0;

                    int targetCol = Math.Abs(coordPairs[i + 1]) - 1;

                    int newSum = 0;
                    for (; col != targetCol; col += horizontalDirection)
                    {
                        newSum += mtrx[row, col];
                    }

                    for (; row != targetRow; row += verticalDirection)
                    {
                        newSum += mtrx[row, col];
                    }

                    newSum += mtrx[row, col];

                    if (newSum > maxSum)
                    {
                        maxSum = newSum;
                    }
                }

                Console.WriteLine(maxSum);
            }

        }

        static int[,] GenerateMatrix(int size)
        {
            var mtrx = new int[0, 0];

            bool isInitated = false;
            for (int i = 0; i < size; i++)
            {

                var currentRow = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

                if (isInitated == false)
                {
                    mtrx = new int[size, currentRow.Length];
                    isInitated = true;
                }

                for (int j = 0; j < mtrx.GetLength(0); j++)
                {
                    mtrx[i, j] = currentRow[j];
                }
            }
            return mtrx;
        }
    }
}